# [Trim] - New feature not available in Python

**Optimizes** a string by cleaning the characters (**"\n"**, **"\t"**, **"\r"**, **"\0"**, **"\x0B"**, **" "**). same principle as the trim() function of PHP.

Complexity of the algorithm : **O(n)**

## Note

The strip() function built into Python removes characters at the beginning and end of a string only, but it doesn't remove characters in the middle, which can be a problem for developers wanting to find a function similar to PHP's trim() function. 

With the trim() function, you can delete at the beginning, middle and end of a string

To date (20 December 2019), no function similar to Trim() of PHP exists in Python.

Trim replaces the functions **strip()**, **lstrip()**, **rstrip()**.

The **"script.py"** file shows examples on the functions **strip()**, **lstrip()**, **rstrip()**, **trim()**


## Example

```python
    #!/usr/bin/python3

    from String import String
    
    message = "   Hello   World   !    "
    
    print(f"Before : {message}")
    print(f"Before : {len(message)}")
    
    trim = String.trim(message)
    
    print(f"\nAfter : {trim}")
    print(f"After : {len(trim)}")
```

## Todo
- [ ] Improves the performance of the algorithm
- [ ] Parallelism with **Cuda** and/or **Numba**
- [x] Deleting unwanted characters (**"\n"**, **"\t"**, **"\r"**, **"\0"**, **"\x0B"**, **" "**)

## Contact

Feel free to contact me if you have any questions, criticisms, suggestions, bugs encountered 

* **Twitter :** https://twitter.com/mxmmoreau
* **LinkedIn :** https://www.linkedin.com/in/mxmoreau/