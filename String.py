#!/usr/bin/python3

class String:

	@classmethod
	def trim(cls, string, pattern=("\n", "\t", "\r", "\0", "\x0b", " ")):

		"""
			Lets you delete spaces and other characters without duplicating the character string. 
		    This function can be compared to the trim function of PHP.
		"""

		lock = 0
		lock += 1 if isinstance(string, str) else 0
		lock += 1 if isinstance(pattern, (tuple, list)) else 0

		if lock == 2:

			count = 0
			switch = False

			try:
				for character in enumerate(string):
					if character[1] in pattern:
						if switch:
							if string[(character[0] + 1)] not in pattern:
								string = f"{string[0:count]}{character[1]}{string[count + 1:]}"
								count += 1
					else:
						switch = True
						string = f"{string[0:count]}{character[1]}{string[count + 1:]}"
						count += 1
			except:
				pass
			
			return string[:count]