import sys
from String import String

print(""" 
		  _______ _____  _____ __  __ 
		 |__   __|  __ \|_   _|  \/  |
		    | |  | |__) | | | | \  / |
		    | |  |  _  /  | | | |\/| |
		    | |  | | \ \ _| |_| |  | |
		    |_|  |_|  \_\_____|_|  |_|\n

	    Version 0.1.1 - Developed By Maxime Moreau
""")
  
# Message
                 
print("\n.:: Message ::.\n")

message = "   Hello   World   !   "

print("\t:" + message)
print("\t:", message.split(" "), "\n")

# strip Function

strip = message.strip()

print("\n.:: STRIP Function ::.\n")
print("\t:", strip)
print("\t:", len(strip))
print("\t:", strip.split(" "), "\n")

# lstrip Function

lstrip = message.lstrip(" ")

print("\n.:: LSTRIP Function ::.\n")
print("\t:", lstrip)
print("\t:", len(lstrip))
print("\t:", lstrip.split(" "), "\n")

# rstrip Function

rstrip = message.rstrip(" ")

print("\n.:: RSTRIP Function ::.\n")
print("\t:", rstrip)
print("\t:", len(rstrip))
print("\t:", rstrip.split(" "), "\n")

# trim Function

trim = String.trim(message)

print("\n.:: TRIM Function ::.\n")
print("\t:", trim)
print("\t:", len(trim))
print("\t:", trim.split(" "), "\n")


if sys.platform.startswith('win32'):
	__import__("os").system("pause")